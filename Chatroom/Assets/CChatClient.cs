using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using TMPro;

public class CChatClient : MonoBehaviour
{
    UdpClient m_theClient = null;
    IPEndPoint m_theEndPoint = null;
    public TMP_InputField userName;
    public TMP_InputField msgInput;

    private void Start() 
    {
        bool bOK = Connect( "127.0.0.1", 4099 ); 

        if( bOK )
        {
            SendName( userName.ToString() );
                
            while( true )
            {
                while (Console.KeyAvailable == false)
                {
                    Run();
                    System.Threading.Thread.Sleep( 1 );
                }
                    SendBroadcast( msgInput.ToString() );
            }
        }   
    }

    public bool Connect( string sAddress, int iPort )
    {
        m_theClient = new UdpClient();

    try
        {
            IPHostEntry host = Dns.GetHostEntry( sAddress );
            var address = ( from h in host.AddressList where h.AddressFamily == AddressFamily.InterNetwork select h ).First();

            m_theEndPoint = new IPEndPoint( address, iPort );
            //m_theClient.Connect( address.ToString(), iPort ); <-- UdpClient also provides connect function as a default end point setting
            //Console.WriteLine( "Connected to Chat Server: " + sAddress + ":" + iPort + "\n" );

            return true;
        }
        catch( Exception e )
        {
            Debug.Log( "Exception happened: " + e.ToString() );
            return false;
        }
    }

    public void SendName( string sName )
    {
        string sRequest = "LOGINNAME:" + sName;
        byte[] aRequestBuffer = System.Text.Encoding.ASCII.GetBytes( sRequest );

        m_theClient.Send( aRequestBuffer, aRequestBuffer.Length, m_theEndPoint );
    }

    public void SendBroadcast( string sMessage )
    {
        string sRequest = "BROADCAST:" + sMessage;
        byte[] aRequestBuffer = System.Text.Encoding.ASCII.GetBytes( sRequest );

        m_theClient.Send( aRequestBuffer, aRequestBuffer.Length, m_theEndPoint );
    }

    public void Run()
    {
        if( m_theClient.Available > 0 )
        {
                _HandleReceiveMessages( m_theClient );
        }
    }

    public void _HandleReceiveMessages( UdpClient theClient )
    {
        IPEndPoint ep = null;
        byte[] aBuffer = theClient.Receive( ref ep );

        string sRequest = System.Text.Encoding.ASCII.GetString( aBuffer );

        if( sRequest.StartsWith( "MESSAGE:", StringComparison.OrdinalIgnoreCase ) )
        {
            string[] aTokens = sRequest.Split( ':' );
            string sName = aTokens[ 1 ];
            string sMessage = aTokens[ 2 ];
            Debug.Log( sName + " said: " + sMessage );
        }
    }
}
