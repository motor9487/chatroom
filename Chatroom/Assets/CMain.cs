using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class CMain : MonoBehaviour
{
    public TMP_InputField userName;
    public TMP_InputField msgInput;

    public void Updata() 
    {
        CChatClient client = new CChatClient();
        bool bOK = client.Connect( "127.0.0.1", 4099 ); 

        if( bOK )
        {
            client.SendName( userName.ToString() );
                
            while( true )
            {
                while (Console.KeyAvailable == false)
                {
                    client.Run();
                    System.Threading.Thread.Sleep( 1 );
                }
                    client.SendBroadcast( msgInput.ToString() );
            }
        }   
    }
}
