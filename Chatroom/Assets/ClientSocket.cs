using System;
using System.Net.Sockets;
using UnityEngine;
using System.Net;
using System.Linq;

public class ClientSocket
{
    UdpClient m_theClient = null;
    IPEndPoint m_theEndPoint = null;
    

    // 連線伺服器
    public void Connect(string sAddress, int port)
    {
        m_theClient = new UdpClient();

        try
        {
 
            IPHostEntry host = Dns.GetHostEntry( sAddress );
            var address = ( from h in host.AddressList where h.AddressFamily == AddressFamily.InterNetwork select h ).First();

            m_theEndPoint = new IPEndPoint( address, port );

        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
        }
    }

    public void SendName( string unameInput )
    {
        string sRequest = "LOGINNAME:" + unameInput;
        byte[] data = System.Text.Encoding.ASCII.GetBytes(sRequest);

        // 傳送帳號給服務端
        m_theClient.Send(data, data.Length, m_theEndPoint);
    }

    public void SendBroadcast(string chatMsgTxt)
    {
        string sRequest = "BROADCAST:" + chatMsgTxt;
        byte[] data = System.Text.Encoding.ASCII.GetBytes(sRequest);

        // 傳送訊息給服務端
        m_theClient.Send(data, data.Length, m_theEndPoint);
    }

    // 從訊息佇列中取出訊息
    public string Run()
    {
        if( m_theClient.Available > 0 )
        {
            return _HandleReceiveMessages( m_theClient );
        }
        return null;
    }

    public string _HandleReceiveMessages( UdpClient theClient )
    {
        IPEndPoint ep = null;
        byte[] aBuffer = theClient.Receive( ref ep );

        string sRequest = System.Text.Encoding.ASCII.GetString( aBuffer );

        if( sRequest.StartsWith( "MESSAGE:", StringComparison.OrdinalIgnoreCase ) )
        {
            string[] aTokens = sRequest.Split( ':' );
            string sName = aTokens[ 1 ];
            string sMessage = aTokens[ 2 ];
            return sName + " said: " + sMessage;
        }
        return null;
    }
}