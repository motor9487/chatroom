using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TestPanel : MonoBehaviour
{
    const string IP = "127.0.0.1";
    const int PORT = 4099;

    public TMP_InputField unameInput;  // 使用者名稱輸入
    public TMP_InputField msgInput;    // 訊息輸入
    public Button loginBtn;            // 登入按鈕
    public Button sendBtn;             // 傳送按鈕
    public TMP_Text chatMsgTxt;        // 聊天室聊天文字
    
    ClientSocket clientSocket = new ClientSocket();

    void Start()
    {
        clientSocket.Connect(IP, PORT);

        loginBtn.onClick.AddListener(() =>
        {
            clientSocket.SendName(unameInput.text);
        });

        sendBtn.onClick.AddListener(() =>
        {
            clientSocket.SendBroadcast(msgInput.text);
        });  
    }

    void Update() 
    {
        var msg = clientSocket.Run();
        if (!string.IsNullOrEmpty(msg))
        {
            chatMsgTxt.text += msg + "\n";
        }
    }
}
